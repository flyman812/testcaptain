import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_captain/Controllers/ServiceController.dart';
import 'package:test_captain/Utils/PageNames.dart';
import 'package:test_captain/models/User.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController nameController = new TextEditingController();
  TextEditingController lastNameController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "Login Page",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      body: GetBuilder<ServiceController>(
        builder: (_) => GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Center(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 32, vertical: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "what is your name:",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    margin: EdgeInsets.symmetric(vertical: 16),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 2, color: Colors.blueGrey.shade400)
                        ]),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: TextField(
                            controller: nameController,
                            decoration: InputDecoration(
                              hintText: "insert your first name",
                            ),
                            autofocus: true,
                            onSubmitted: (v) {
                              if (v.length > 3) {
                                FocusScope.of(context).unfocus();
                                User user = new User();
                                user.name = v;
                                _.storeUserData(userData: user);
                                Get.offAllNamed(NamePages.homePage);
                              } else {
                                Get.snackbar(
                                  "wrong name",
                                  "please insert correct name",
                                  backgroundColor: Colors.redAccent,
                                  colorText: Colors.white,
                                );
                              }
                            },
                          ),
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.done,
                            color: Colors.blueAccent,
                          ),
                          onPressed: () {
                            FocusScope.of(context).unfocus();
                            if (nameController.value.text.length > 3) {
                              FocusScope.of(context).unfocus();
                              User user = new User();
                              user.name = nameController.value.text;
                              _.storeUserData(userData: user);
                              Get.offAllNamed(NamePages.homePage);
                            } else {
                              Get.snackbar(
                                "wrong name",
                                "please insert correct name",
                                backgroundColor: Colors.redAccent,
                                colorText: Colors.white,
                              );
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
