import 'package:get/get.dart';
import 'package:test_captain/HomePage.dart';
import 'package:test_captain/LoginPage.dart';
import 'package:test_captain/PostDetailsPage.dart';
import 'package:test_captain/SplashScreen.dart';

List<GetPage> listPages = [
  GetPage(name: NamePages.homePage, page: () => HomePage()),
  GetPage(name: NamePages.postDetails, page: () => PostDetailsPage()),
  GetPage(name: NamePages.splashScreen, page: () => SplashScreen()),
  GetPage(name: NamePages.loginPage, page: () => LoginPage()),
];

class NamePages {
  static const String homePage = "/homePage";
  static const String postDetails = "/postDetails";
  static const String splashScreen = "/SplashScreen";
  static const String loginPage = "/LoginPage";
}
