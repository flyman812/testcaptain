import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_captain/Utils/Urls.dart';
import 'package:test_captain/models/User.dart';
import 'package:test_captain/models/comment.dart';

import 'package:test_captain/models/post.dart';

class ServiceController extends GetxController {
//variables
  List<Post> _listPosts = new List();
  List<Post> _searchPost = new List();
  List<Post> _cachePost = new List();

  bool _hasDataCache = false;
  bool _hasInternet = true;
  User _user = new User();
  bool _sendComment = false;
  bool _searchSomethings = false;
  bool _offlineMode = false;

// getters

  bool get offlineMode => _offlineMode;

  List<Post> get cachePost => _cachePost;

  List<Post> get searchPost => _searchPost;

  bool get searchSomethings => _searchSomethings;

  bool get sendComment => _sendComment;

  User get user => _user;

  bool get hasInternet => _hasInternet;

  bool get hasDataCache => _hasDataCache;

  List<Post> get listPosts => _listPosts;

// setter

  set offlineMode(bool offlineMode) {
    _offlineMode = offlineMode;
    update();
  }

  set cachePost(List<Post> cachePost) {
    _cachePost = cachePost;
    update();
  }

  set searchPost(List<Post> searchPost) {
    _searchPost = searchPost;
    update();
  }

  set searchSomethings(bool searchSomethings) {
    _searchSomethings = searchSomethings;
    update();
  }

  set sendComment(bool sendComment) {
    _sendComment = sendComment;
    update();
  }

  set user(User user) {
    _user = user;
    update();
  }

  set listPosts(List<Post> listPosts) {
    _listPosts = listPosts;
    update();
  }

  set hasDataCache(bool hasDataCache) {
    _hasDataCache = hasDataCache;
    update();
  }

  set hasInternet(bool hasInternet) {
    _hasInternet = hasInternet;
    update();
  }

// functions
  Future<List<Post>> getPosts(
      {int idPage, int count, bool update = false}) async {
    var url = idPage != null && count != null
        ? Uri.parse(Urls.baseUrl + Urls.posts + "?page=$idPage&limit=$count")
        : Uri.parse(Urls.baseUrl + Urls.posts);

    try {
      http.Response response = await http.get(url);
      var jsonResponse = jsonDecode(response.body);

      List<Post> posts = List.from(jsonResponse.map((x) => Post.fromJson(x)));
      if (cachePost.length == 0) {
        await storePostToCache(posts: posts);
      } else {
        cachePost.forEach((element) {
          Post post = listPosts.firstWhere((server) => server.id != element.id);
          cachePost.add(post);
        });
        await storePostToCache(posts: cachePost);
      }
      if (listPosts.length == 0 || update) {
        listPosts = posts != null ? posts : null;
      } else {
        posts.forEach((server) {
          Post post = listPosts.firstWhere((store) => store.id != server.id);
          listPosts.add(post);
        });
      }
      return listPosts;
    } on Exception catch (e) {
      print(e);
      return null;
    }
  }

  Future<List<Post>> getPostsFromCache() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();

      var posts;
      final String postStr = prefs.getString("posts");
      posts = postStr != null ? jsonDecode(postStr) as List : null;

      final List<Post> allPosts = posts != null
          ? posts.map((i) => Post.fromJson(i)).toList()
          : new List<Post>();
      hasDataCache = allPosts.length > 0 ? true : false;
      cachePost = allPosts;

      return allPosts;
    } on Exception catch (e) {
      print(e);
      hasDataCache = false;
      return null;
    }
  }

  Future<bool> storePostToCache({List<Post> posts}) async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      var postStr = jsonEncode(posts);

      bool result = await prefs.setString("posts", postStr);

      return result;
    } on Exception catch (e) {
      print(e);
      return null;
    }
  }

  Future<bool> checkInternetConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        hasInternet = true;
        return true;
      }
    } on SocketException catch (_) {
      print('not connected');
      hasInternet = false;
      return false;
    }
  }

  Future<User> getUserFromCache() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();

      var userData;
      final String userStr = prefs.getString("user");
      userData = userStr != null ? jsonDecode(userStr) : null;

      user = userData != null ? User.fromJson(userData) : null;

      return user;
    } on Exception catch (e) {
      print(e);
      hasDataCache = false;
      return null;
    }
  }

  Future<bool> storeUserData({User userData}) async {
    try {
      user = userData;
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      var userStr = jsonEncode(userData);

      bool result = await prefs.setString("user", userStr);

      return result;
    } on Exception catch (e) {
      print(e);
      return null;
    }
  }

  Future<bool> sendCommentMethod(
      {@required Comment comment, @required String postID}) async {
    var url = Uri.parse(Urls.baseUrl + Urls.posts + "/$postID/comment");
    http.Response response = await http.post(url, body: {
      "user": comment.user,
      "body": comment.body,
    });
    if (response.statusCode == 200) {
      var jsonData = jsonDecode(response.body);
      return true;
    } else {
      return false;
    }
  }

  Future<List<Post>> searchMethod({String title}) async {
    var url = Uri.parse(Urls.baseUrl + Urls.posts + "?title=$title");

    http.Response response = await http.get(url);

    if (response.statusCode == 200) {
      var jsonData = jsonDecode(response.body);
      List<Post> posts = List.from(jsonData.map((x) => Post.fromJson(x)));
      return posts;
    } else {
      return null;
    }
  }
}
