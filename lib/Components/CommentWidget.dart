
import 'package:flutter/material.dart';
import 'package:test_captain/models/comment.dart';

class CommentWidget extends StatelessWidget {
  CommentWidget({this.comment});
  final Comment comment;  
  
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 8,
      ),
      color: Colors.blueGrey.shade300,
      child: Container(
        padding: EdgeInsets.all(8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(comment.user,style: TextStyle(
                  color: Colors.blueGrey.shade700,
                  fontWeight: FontWeight.bold
                ),),
                Text(comment.createdAt.toUtc().toString().substring(0,10).replaceAll("-","/")),
              ],
            ),
            Text(comment.body,
            maxLines: 100,
            style: TextStyle(
              color: Colors.white70,
            ),)
          ],
        ),
      ),
    );
  }
}