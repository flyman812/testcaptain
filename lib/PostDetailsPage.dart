import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:test_captain/Components/CommentWidget.dart';
import 'package:test_captain/Controllers/ServiceController.dart';
import 'package:test_captain/models/comment.dart';
import 'package:test_captain/models/post.dart';
import 'package:keyboard_utils/keyboard_listener.dart';
import 'package:keyboard_utils/keyboard_utils.dart';

class PostDetailsPage extends StatefulWidget {
  @override
  _PostDetailsPageState createState() => _PostDetailsPageState();
}

class _PostDetailsPageState extends State<PostDetailsPage> {
  TextEditingController controller = new TextEditingController();
  ScrollController scrollController = new ScrollController();
  ServiceController sc = Get.put(ServiceController());
  Post post;

  @override
  void initState() {
    if (Get.arguments != null) {
      post = Get.arguments;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return GetBuilder<ServiceController>(
      builder: (_) => Scaffold(
        appBar: AppBar(
          title: Text(
            post.title,
            style: TextStyle(color: Colors.white),
          ),
          bottom: PreferredSize(
              child: Container(
                width: width,
                height: 50,
                padding: EdgeInsets.all(8),
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("views: ${post.views}"),
                    Text(
                        "created At: ${post.createdAt.toUtc().toString().substring(0, 10)}"),
                  ],
                ),
              ),
              preferredSize: Size(width, 50)),
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          onHorizontalDragUpdate: (details) {
            if (details.primaryDelta > 10) {
              Get.back();
            }
          },
          child: ListView.builder(
              padding: EdgeInsets.symmetric(vertical: 8),
              itemCount: post.comments.length,
              controller: scrollController,
              itemBuilder: (context, index) {
                return post.comments.length == index + 1
                    ? SizedBox(
                        height: 50,
                      )
                    : CommentWidget(
                        comment: post.comments[index],
                      );
              }),
        ),
        bottomSheet: Container(
          width: width,
          padding: EdgeInsets.symmetric(horizontal: 8),
          color: Colors.white,
          child: Row(
            children: [
              Expanded(
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  controller: controller,
                  onTap: () {
                    scrollController.position.jumpTo(
                      scrollController.position.maxScrollExtent,
                    );
                    setState(() {});
                  },
                  onFieldSubmitted: (value) {
                    addNewComment(value: value);
                  },
                  decoration: InputDecoration(
                    hintText: "add comment...",
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  addNewComment(value: controller.value.text);
                },
                child: _.sendComment
                    ? SpinKitCircle(
                        color: Colors.blueAccent,
                        size: 20,
                      )
                    : Icon(
                        Icons.arrow_forward_ios_sharp,
                        color: Colors.lightBlue,
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void addNewComment({String value}) {
    Comment comment = new Comment();
    value.length > 0
        ? comment.body = value
        : Get.snackbar("No text", "please write somethings");
    comment.createdAt = DateTime.now();
    comment.user = sc.user.name;
    sc.sendComment = true;
    sc.sendCommentMethod(comment: comment, postID: post.id).then((value) {
      sc.sendComment = false;
      if (value != null) {
        post.comments.add(comment);
      }
    });
    sc.sendComment = false;
    controller.value = TextEditingValue.empty;
    FocusScope.of(context).unfocus();
  }
}
