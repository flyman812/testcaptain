import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_captain/Controllers/ServiceController.dart';
import 'package:test_captain/Utils/PageNames.dart';
import 'package:test_captain/models/post.dart';
import 'package:test_captain/res/Assets.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  ServiceController sc = Get.put(ServiceController());

  @override
  void initState() {
    sc.checkInternetConnection().then((value) {
      if (value) {
        sc.getPosts().then((value) {
          value != null
              ? sc.getUserFromCache().then((value) {
                  value != null
                      ? Get.offAllNamed(NamePages.homePage)
                      : Get.offAllNamed(NamePages.loginPage);
                })
              : printError();
        });
      } else {
        Get.snackbar("check Internet",
            "please check internet connection or go offline mode",
            icon: Icon(Icons.network_check_rounded, color: Colors.redAccent));
        sc.getPostsFromCache().then((value) {
          sc.offlineMode = value != null ? true : false;
          value != null
              ? Get.offAllNamed(NamePages.homePage)
              : Get.snackbar("No Cache Data", "you dont have any cache data");
        });
      }
    });
    super.initState();
  }

  final planet = FlareActor(
    Assets.planet,
    alignment: Alignment.center,
    animation: "go",
    fit: BoxFit.contain,
  );

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return GetBuilder<ServiceController>(
      builder: (_) => Scaffold(
        backgroundColor: Colors.white,
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Captain Blog",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 35,
                ),
              ),
              SizedBox(
                width: width * 0.8,
                height: width * 0.8,
                child: planet,
              ),
              Visibility(
                visible: !_.hasInternet && _.hasDataCache,
                child: GestureDetector(
                  onTap: () {
                    _.getUserFromCache().then((value) {
                      sc.offlineMode = value != null ? true : false;
                      value != null
                          ? Get.offAllNamed(NamePages.homePage)
                          : Get.offAllNamed(NamePages.loginPage);
                    });
                  },
                  child: AnimatedContainer(
                    width: !_.hasInternet && _.hasDataCache ? 150 : 0,
                    height: !_.hasInternet && _.hasDataCache ? 35 : 0,
                    duration: Duration(
                      milliseconds: 300,
                    ),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: Colors.redAccent,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.redAccent,
                          blurRadius: 5,
                          spreadRadius: 2,
                        ),
                      ],
                    ),
                    child: Text(
                      "Go Offline",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
