


class Comment {

    String id;
    String postId;
    DateTime createdAt;
    String user;
    String body;

    Comment();
    Comment.fromJson(Map<String, dynamic> json){
        id= json["id"];
        postId= json["postId"];
        createdAt= DateTime.parse(json["createdAt"]);
        user= json["user"];
        body= json["body"];
    }

    Map<String, dynamic> toJson() => {
        "id": id,
        "postId": postId,
        "createdAt": createdAt.toIso8601String(),
        "user": user,
        "body": body,
    };
}