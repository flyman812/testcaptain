import 'package:test_captain/models/comment.dart';

class Post {

    String id;
    DateTime createdAt;
    int views;
    bool published;
    String title;
    List<Comment> comments;

    Post();
    Post.fromJson(Map<String, dynamic> json) {
        id= json["id"];
        createdAt= DateTime.parse(json["createdAt"]);
        views= json["views"];
        published= json["published"];
        title= json["title"];
        comments= List<Comment>.from(json["comments"].map((x) => Comment.fromJson(x)));
    }

    Map<String, dynamic> toJson() => {
        "id": id,
        "createdAt": createdAt.toIso8601String(),
        "views": views,
        "published": published,
        "title": title,
        "comments": List<dynamic>.from(comments.map((x) => x.toJson())),
    };
}