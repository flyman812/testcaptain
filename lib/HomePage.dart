import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:test_captain/Components/PostWidget.dart';
import 'package:test_captain/Controllers/ServiceController.dart';
import 'package:test_captain/models/post.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ServiceController sc = Get.put(ServiceController());
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  ScrollController scrollController = new ScrollController();
  TextEditingController searchController = new TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return GetBuilder<ServiceController>(
      builder: (_) => Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.amber,
          title: Text(
            "Home Page",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          actions: [
            !sc.hasInternet
                ? Container(
                    width: 35,
                    height: 35,
                    margin: EdgeInsets.symmetric(horizontal: 8),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.redAccent,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.redAccent,
                            blurRadius: 5,
                            spreadRadius: 2,
                          )
                        ]),
                    child: Icon(
                      Icons.network_check_rounded,
                      color: Colors.white,
                    ))
                : SizedBox(),
          ],
          bottom: PreferredSize(
              child: Container(
                padding: EdgeInsets.all(8),
                child: Row(
                  children: [
                    Expanded(
                      child: TextField(
                        controller: searchController,
                        decoration: InputDecoration(
                          hintText: "search your post...",
                          hintStyle: TextStyle(
                            color: Colors.white60,
                          ),
                        ),
                        onSubmitted: (v) {
                          searchMethod(value: v);
                        },
                        onChanged: (v) {
                          if (v.length == 0) {
                            sc.searchPost.clear();
                          }
                        },
                      ),
                    ),
                    !sc.searchSomethings
                        ? IconButton(
                            icon: Icon(
                              Icons.search,
                              color: Colors.blueAccent,
                            ),
                            onPressed: () {
                              searchMethod(value: searchController.text);
                            },
                          )
                        : SpinKitCircle(
                            color: Colors.blueAccent,
                            size: 25,
                          ),
                  ],
                ),
              ),
              preferredSize: Size(width, 50)),
        ),
        body: RefreshIndicator(
          key: _refreshIndicatorKey,
          onRefresh: () => _.checkInternetConnection().then((value) => value
              ? sc.getPosts(count: sc.listPosts.length, idPage: 1, update: true)
              : Get.snackbar(
                  "network Problem", "you dont have internet connection")),
          child: !sc.searchSomethings
              ? !sc.offlineMode && sc.hasInternet
                  ? ListView.builder(
                      itemCount: _.searchPost.length > 0
                          ? _.searchPost.length
                          : _.listPosts.length,
                      controller: scrollController,
                      itemBuilder: (context, index) {
                        Post post = _.searchPost.length > 0
                            ? _.searchPost[index]
                            : sc.listPosts[index];
                        return PostWidget(
                          post: post,
                        );
                      })
                  : ListView.builder(
                      itemCount: _.cachePost.length,
                      itemBuilder: (context, index) {
                        Post post = _.cachePost[index];
                        return PostWidget(
                          post: post,
                        );
                      })
              : Center(
                  child: SpinKitCubeGrid(
                    color: Colors.blueAccent,
                    size: 45,
                  ),
                ),
        ),
      ),
    );
  }

  void searchMethod({String value}) {
    String title;

    value.length > 0
        ? title = value
        : Get.snackbar("No result", "please write somethings in search");
    sc.searchSomethings = true;
    sc.searchMethod(title: title).then((value) {
      sc.searchSomethings = false;
      if (value != null) {
        sc.searchPost = value;
      } else {
        List<Post> emptyPost = new List();
        sc.searchPost = emptyPost;
      }
    });
    sc.sendComment = false;
    FocusScope.of(context).unfocus();
  }
}
